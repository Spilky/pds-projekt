/**
 * Created by David Spilka (xspilk00) on 11.4.16.
 */

function forWindowGraph(data) {
    var forWindow = [];
    $.each(data, function(no, size) {
        forWindow.push([parseFloat(no), size])
    });
    return forWindow;
}

$(function () {
    $('.sender-mac').text(macs.sender);
    $('.receiver-mac').text(macs.receiver);
    $('.sender-ip').text(ips.sender);
    $('.receiver-ip').text(ips.receiver);
    $('.sender-port').text(ports.sender);
    $('.receiver-port').text(ports.receiver);

    $('.packets-count').text(stats.packetsCount);
    $('.data-size').text(stats.dataSize + ' B');
    $('.communication-time').text(stats.communicationTime + ' s');

    $('.sender-slow-start').text(slow_start.sender);
    $('.receiver-slow-start').text(slow_start.receiver);

    $('#window-sender').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Time [s]'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Window size [B]'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' B'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sender',
            data: forWindowGraph(window_size['sender'])
        }]
    });
    
    $('#window-receiver').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Time [s]'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Window size [B]'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' B'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Receiver',
            data: forWindowGraph(window_size['receiver'])
        }]
    });

    $('#speed-sender').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Time [s]'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Size of packets [B]'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' B'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sender',
            data: forWindowGraph(speed['sender'])
        }]
    });

    $('#speed-receiver').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Time [s]'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Size of packets [B]'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' B'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Receiver',
            data: forWindowGraph(speed['receiver'])
        }]
    });

    $('#rtt-sender').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Sequence number'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Time'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'B'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sender',
            data: forWindowGraph(rtt['sender'])
        }]
    });

    $('#rtt-receiver').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Sequence number'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Time'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'B'
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Receiver',
            data: forWindowGraph(rtt['receiver'])
        }]
    });

    $('#sequence-sender').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Time [s]'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Sequence number'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sender',
            data: forWindowGraph(sequence_numbers['sender'])
        }]
    });

    $('#sequence-receiver').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: 'Time [s]'
            },
            type: 'linear'
        },
        yAxis: {
            title: {
                text: 'Sequence number'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sender',
            data: forWindowGraph(sequence_numbers['receiver'])
        }]
    });

    $.each(packets_list, function(i, packet) {
        $("#packets-list").find('tbody')
            .append($('<tr>')
                .append($('<td>').text(packet.no))
                .append($('<td>').text(packet.src))
                .append($('<td>').text(packet.dst))
                .append($('<td>').text(packet.time))
                .append($('<td>').text(packet.size))
            );
    });

    $("#packets-list").bootgrid();
});