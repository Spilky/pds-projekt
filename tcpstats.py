#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import json
import math
from decimal import *

if len(sys.argv) != 2:
    sys.stderr.write("Špatný počet parametrů.\n")
    exit(1)

try:
    packets = rdpcap(sys.argv[1])
except:
    sys.stderr.write("Soubor se nepodařilo načíst.\n")
    exit(1)

# -------------
# PARSE PACKETS
# -------------
print 'Parsuji pakety'
startTime = endTime = -1
macs = {}
ips = {}
ports = {}
stats = {'packetsCount': 0, 'dataSize': 0, 'communicationTime': 0}
TCPPackets = []
TCPPacketsList = []
i = 0
for packet in packets:
    i += 1
    if TCP in packet:
        if startTime == -1:
            startTime = Decimal(packet.time)
        endTime = Decimal(packet.time)
        if not 'sender' in macs:
            macs['sender'] = packet[Ether].src
        elif not 'receiver' in macs:
            macs['receiver'] = packet[Ether].src
        if not 'sender' in ips:
            ips['sender'] = packet[IP].src
        elif not 'receiver' in ips:
            ips['receiver'] = packet[IP].src
        if not 'sender' in ports:
            ports['sender'] = packet[TCP].sport
        elif not 'receiver' in ports:
            ports['receiver'] = packet[TCP].sport
        TCPPackets.append(packet)
        TCPPacketsList.append({
            'no': i,
            'src': packet[IP].src,
            'dst': packet[IP].dst,
            'time': packet.time,
            'size': len(packet)
        })
        stats['packetsCount'] += 1
        stats['dataSize'] += len(packet)
stats['communicationTime'] += float(endTime - startTime)

# -----------
# WINDOW SIZE
# -----------
print 'Analyzuji velikost okna'
windowSize = {'sender': {}, 'receiver': {}}
scales = {}
i = 0
for packet in TCPPackets:
    if packet[IP].src == ips['sender']:
        key = 'sender'
    else:
        key = 'receiver'
    time = Decimal(packet.time) - startTime
    if i < 2:
        options = dict(packet[TCP].options)
        if 'WScale' in options:
            scales[packet[IP].src] = math.pow(2, options['WScale'])
        else:
            scales[packet[IP].src] = 1
        windowSize[key][str(float(time))] = packet[TCP].window
    else:
        windowSize[key][str(float(time))] = packet[TCP].window * scales[packet[IP].src]
    i += 1

# -------------
# SENDING SPEED
# -------------
print 'Analyzuji rychlost přenosu'
speed = {'sender': {0: 0}, 'receiver': {0: 0}}
actualTime = 0
while actualTime < float(endTime - startTime):
    actualTime += 1
    speed['sender'][str(actualTime)] = 0
    speed['receiver'][str(actualTime)] = 0

actualFirst = startTime
for packet in TCPPackets:
    if packet[IP].src == ips['sender']:
        key = 'sender'
    else:
        key = 'receiver'
    time = Decimal(packet.time) - startTime
    speed[key][str(int(math.floor(time)+1))] += len(packet)

# ---------------
# ROUND TRIP TIME
# ---------------
print 'Analyzuji round trip time'
rtt = {'sender': {}, 'receiver': {}}
senderStart = None
receiverStart = None
senderPrev = None
receiverPrev = None
for packet in TCPPackets:
    if packet[IP].src == ips['receiver']:
        if senderPrev is not None:
            if senderPrev[TCP].flags == 18 or senderPrev[TCP].flags == 2:
                tcpLen = 1
            else:
                padding = len(senderPrev[TCP].payload) - (senderPrev[IP].len - (senderPrev[TCP].dataofs * 4) - (senderPrev[IP].ihl * 4))
                tcpLen = len(senderPrev[TCP].payload) - padding
            if senderPrev[TCP].seq + tcpLen == packet[TCP].ack and senderPrev[TCP].ack == packet[TCP].seq:
                rtt['sender'][str(senderPrev[TCP].seq + tcpLen)] = float(Decimal(packet.time) - Decimal(senderStart.time))
            senderStart = None
            senderPrev = None
        if receiverStart is None:
            receiverStart = packet
        receiverPrev = packet
    else:
        if receiverPrev is not None:
            if receiverPrev[TCP].flags == 18 or receiverPrev[TCP].flags == 2:
                tcpLen = 1
            else:
                padding = len(receiverPrev[TCP].payload) - (receiverPrev[IP].len - (receiverPrev[TCP].dataofs * 4) - (receiverPrev[IP].ihl * 4))
                tcpLen = len(receiverPrev[TCP].payload) - padding
            if receiverPrev[TCP].seq + tcpLen == packet[TCP].ack and receiverPrev[TCP].ack == packet[TCP].seq:
                rtt['receiver'][str(receiverPrev[TCP].seq + tcpLen)] = float(Decimal(packet.time) - Decimal(receiverStart.time))
            receiverStart = None
            receiverPrev = None
        if senderStart is None:
            senderStart = packet
        senderPrev = packet

# ----------------
# SEQUENCE NUMBERS
# ----------------
print 'Analyzuji sekvenční čísla'
sequenceNumbers = {'sender': {}, 'receiver': {}}
mss = {}
prev = {'sender': None, 'receiver': None}
slowStart = {'sender': 'Ano', 'receiver': 'Ano'}
i = 0
for packet in TCPPackets:
    if packet[IP].src == ips['sender']:
        key = 'sender'
    else:
        key = 'receiver'
    if i < 2:
        options = dict(packet[TCP].options)
        if 'MSS' in options:
            mss[packet[IP].src] = options['MSS']
    elif (i > 4) and (i < len(TCPPackets)-2):
        if packet[IP].src in mss and slowStart[key] == 'Ano':
            if prev[key][TCP].seq + mss[packet[IP].src] != packet[TCP].seq and prev[key][TCP].seq != packet[TCP].seq:
                slowStart[key] = 'Ne'
    time = Decimal(packet.time) - startTime
    sequenceNumbers[key][str(float(time))] = packet[TCP].seq
    prev[key] = packet
    i += 1

print 'Zapisuji výsledky analýz do log souboru'
file_ = open('./log/log.js', 'w')
file_.write("var macs = " + json.dumps(macs, sort_keys=True) + ";\n")
file_.write("var ips = " + json.dumps(ips, sort_keys=True) + ";\n")
file_.write("var ports = " + json.dumps(ports, sort_keys=True) + ";\n")
file_.write("var stats = " + json.dumps(stats, sort_keys=True) + ";\n")
file_.write("var packets_list = " + json.dumps(TCPPacketsList, sort_keys=True) + ";\n")
file_.write("var window_size = " + json.dumps(windowSize, sort_keys=True) + ";\n")
file_.write("var speed = " + json.dumps(speed, sort_keys=True) + ";\n")
file_.write("var rtt = " + json.dumps(rtt, sort_keys=True) + ";\n")
file_.write("var sequence_numbers = " + json.dumps(sequenceNumbers, sort_keys=True) + ";\n")
file_.write("var slow_start = " + json.dumps(slowStart, sort_keys=True) + ";\n")
file_.close()
